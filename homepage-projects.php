<?php

function homeProjects(){

echo '<div class="grid">';
  
query_posts(array(
	'post_type' => 'fproject',
	'order' => 'DESC',
	'posts_per_page' => 2,
));
if (have_posts()) : while (have_posts()) : the_post();

echo '<figure class="effect-hera"><a href="';
the_permalink();
echo '">';

if ( has_post_thumbnail() ) {
	the_post_thumbnail( 'large' );
} 
      
echo '<figcaption><h2>';
the_title();	
echo '<span>view</span></h2></figcaption></a></figure>';
   
endwhile;
endif;
wp_reset_query();

echo '</div>';
}
?>