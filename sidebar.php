<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div id="primary" class="widget-area" role="complementary">
<?php 
if(is_page(143) || is_page(145)){
echo '<ul class="prof">';
  dynamic_sidebar( 'prof-widget-area' ); 
echo '</ul>';
}else{
echo '<ul class="xoxo">';
  dynamic_sidebar( 'primary-widget-area' ); 
echo '</ul>';
}

?>
</div><!-- #primary .widget-area -->

