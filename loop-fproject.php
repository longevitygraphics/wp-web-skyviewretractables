<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h1 class="entry-title">Featured Project</h1>

<div class="content-column one_half">
<h3><?php the_title(); ?></h3>

<?php
$builder = get_field(fproject_builder); 
if($builder!=''){
	echo '<strong>Builder:</strong> ' . $builder . '<br/>';
}else{}

$type = get_field(fproject_type); 
if($type!=''){
	echo '<strong>Type of Project:</strong> ' . $type . '<br/>';
}else{}

$date = get_field(fproject_date); 
$date = date("F Y", strtotime($date));
if($date!=''){
	echo '<strong>Date Completed:</strong> ' . $date . '<br/>';
}else{}

$desc = get_field(fproject_desc); 
if($desc!=''){
	echo '<h4>Project Description</h4> ' . $desc;
}else{}

$goals = get_field(fproject_goals); 
if($goals!=''){
	echo '<h4>Goals</h4> ' . $goals;
}else{}
?>
</div>
<div class="content-column one_half last_column">
<?php if ( has_post_thumbnail() ) {
	the_post_thumbnail( 'large' );
} 
?>
</div>
<div class="clear_column"></div>

<?php the_content(); ?>

				</div><!-- #post-## -->

<?php endwhile; // end of the loop. ?>