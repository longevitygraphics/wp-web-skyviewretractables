<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
	$date = getdate();
	$year = $date['year']; 
?>
	</div><!-- #main -->
</div><!-- #wrapper -->

<div id="footwrap">
	<div id="footer" role="contentinfo">
	<?php dynamic_sidebar( 'footer-widget-area' ); ?>
				<div class="legal">
				&copy; <?php echo("$year"); ?> Copyright <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php bloginfo( 'name' ); ?>
				</a>
<div class="right">
<a target="_blank" href="http://www.longevitygraphics.com">Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a> | <a href="/privacy-policy">Privacy Policy</a>
</div>
				</div>
	</div><!-- #footer -->
</div><!-- #footwrap -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74630100-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>