<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="jby62CSvljqCaB28zshoOI_vJk0IRBirbXz_5DVCIrc" />
<title><?php
    /*
     * Print the <title> tag based on what is being viewed.
     * We filter the output of wp_title() a bit -- see
     * twentyten_filter_wp_title() in functions.php.
     */
    wp_title( '|', true, 'right' );
    ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //bloginfo( 'stylesheet_url' ); ?>" />-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" media="screen and (min-device-width: 481px)" type="text/css" />
<link type="text/css" rel="stylesheet" media="only screen and (max-device-width: 480px)" href="<?php echo dirname(get_stylesheet_uri()); ?>/phone_style.css" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, 
 maximum-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/parallax.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/css_browser_selector.min.js"></script>

<?php
?>
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>"  media="screen" />
<![endif]-->
 <?php
 
    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
?>
<script>
$(document).ready(function () {

//JS FOR RESPONSIVE TABLES
var tables = document.querySelectorAll(".responsive"),
    tbody, headers, headertext, i, j, k;

for (i = 0; i < tables.length; i++) {

    tbody = tables[i].tBodies[0];
    headers = tables[i].tHead.rows[0].children;
    headertext = [];

    for (j = 0; j < headers.length; j++) {
        headertext.push(headers[j].textContent.trim());
    }

    for (j = 0; j < tbody.rows.length; j++) {
        for (k = 0; k < tbody.rows[j].cells.length; k++) {
            tbody.rows[j].cells[k].setAttribute("data-th", headertext[k]);
        }
    }    
}

});
</script>
<style>
#html5-watermark{display:none !important;}
.wonderplugin-gridgallery-item.wonderplugin-gridgallery-item-visible a{display:none !important;}
.wonderplugin-gridgallery-item.wonderplugin-gridgallery-item-visible .wonderplugin-gridgallery-item-container a{display:block !important;}
</style>
</head>
<body <?php body_class(); ?>>
<div id="headwrap">
    <div id="header" class="clearfix">

<div itemscope itemtype="http://schema.org/LocalBusiness"><div id="topbar"><a href="mailto:info@skyviewretractables.com" id="email" itemprop="email">info@skyviewretractables.com</a> <a href="tel:919-418-1760" id="tel"><span itemprop="telephone">(919) 418-1760</span></a></div></div>

<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php if(is_front_page()){?><h1>Skyview Retractables</h1><?php } ?><img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_new.png" /></a>

            <div id="access" role="navigation">
              <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
                <div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
                <?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
                <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
            </div><!-- #access -->

    </div><!-- #header -->
</div><!-- #headwrap -->

<?php if(is_front_page()){ echo do_shortcode("[metaslider id=71]"); } ?>

<?php if(!is_front_page() && is_page() || is_blog() || is_single()){
if ( has_post_thumbnail() ) {
    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
        echo '<div class="cat-image-wrap"><div data-image-src="' . $large_image_url[0] . '" data-parallax="scroll" class="parallax-window" id="plax-slide"></div></div>';
}else{
        echo '<div class="cat-image-wrap"><div data-image-src="/wp-content/uploads/2016/01/page-default.jpg" data-parallax="scroll" class="parallax-window" id="plax-slide"></div></div>';
} } ?>

<div id="wrapper" class="hfeed">
    <div id="main">
